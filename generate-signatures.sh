#!/bin/sh
#program vars
KEYID='security@tranit.us'
GPG_BIN=gpg

for FILE in $(ls *.keys); do
GPG_SWITCH="-u $KEYID --output $FILE.sig --detach-sig $FILE"
$GPG_BIN $GPG_SWITCH
done
